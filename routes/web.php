<?php

use Illuminate\Support\Facades\Route;
use App\Events\FormSubmitted;
// use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('chat', function () {
    return view('index');
});

Route::get('kirim', function () {
    return view('kirim');
});

Route::post('kirim', function () {
    $text = request()->text;
    // dd($text);
    event(new FormSubmitted($text));
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('send-chat', 'SendChatController@index');
    Route::post('send-chat', 'SendChatController@store');
    Route::post('receive-chat', 'SendChatController@receiveChat');
    Route::get('chat-not-send', 'SendChatController@chatNotSend');
    Route::post('chat-not-send', 'SendChatController@moveChat');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('cookie', function (Request $r) {
    $result = [
        'namauser1' => [
        'name' => 'adib',
        'dari' => 'purwodadi'
        ]
    ];

    // $chat_all = json_decode($r->cookie('data_user'), true);
    // // $chat_all = (array)$chat_all;
    // dd($chat_all);  

    // if($chat_all == null)
    // {
    //     return 'null';
    // }
    // else
    // {
    //     for ($i=38; $i < 40; $i++) { 
    //         $chat_all['nama_user'.$i] = [
    //             'name'.$i => 'adib'.$i,
    //             'dari'.$i => 'purwodadi'.$i
    //         ];
    //     }

    //     // dd($chat_all);

    // }
    
        $minutes = 60;
        $response = new Response();
        $response->withCookie(cookie('data_user', json_encode($result), $minutes));
        return $response;


    // $value = $r->cookie('nama1');
    // echo $value;
});

Route::get('all-chat-user', function () {
    
});

Route::get('set-chat-user/{number}', function ($no) {
    $chat_all = json_decode($_COOKIE['2&1'.$no]);
    $chat_all = (array)$chat_all;

    if($chat_all != null)
    {
        dd($chat_all);
        

        // $minutes = 60;
        // $response = new Response();
        // $response->withCookie(cookie('data_user', json_encode($result), $minutes));
        // return $response;
        // $response = new Response();
        // $response->withCookie(cookie(''))
    } 
    else 
    {
        dump('chat null');
    }
});

Route::get('sizecookie', function () {
    if (isset($_COOKIE["muslikhuladibToalpin2"])) {
        $data = $_COOKIE["muslikhuladibToalpin2"];
        $size = strlen(serialize($data));
        // $size = strlen($serialized_data);
        dd($data);
    }
      
});

Route::get('pusher', function () {
    $app_id = '1048675';
    $app_key = '9fc9cf0e1a216c7f7b7c';
    $app_secret = 'e4e55c7a20becdade4b3';
    $app_cluster = 'ap1';

    $pusher = new Pusher\Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

    dd($pusher->get_channel_info('alpin'));

});