<!DOCTYPE html>
<html>

<head>
    <title>Chat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
        integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js">
    </script>
    <link rel="stylesheet" href="{{ asset('assets/css/style-send-chat.css') }}">
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="{{ asset('assets/js/script-send-chat.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>

 
    <script>



        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.get("chat-not-send", function(data, status){
            console.log(data);
            data = Object.entries(data);
            let amountData = data.length;
            

            for (let i = 0; i < amountData; i++) 
            {
                $.post('chat-not-send', data[i][1], function (data) 
                {
                    console.log(data[i][1]);
                    
                })
                .fail(function () 
                {
                    alert("error");
                });
            }

            // console.log(data[0]);
            // console.log(data[1]);
            console.log();
            
        });
        

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('9fc9cf0e1a216c7f7b7c', {
            cluster: 'ap1'
        });

        console.log(pusher.disconnect());
        
        

        var user = "<?php echo Auth()->user()->name; ?>"; 
            user = user.replace(' ', '');
        var channel = pusher.subscribe(user);
        channel.bind('send-chat', function (data) {
            // alert(JSON.stringify(data));
            // console.log(data['data']['message']);
             
            
            $.post('/receive-chat', data, function (data) {
                let html = `
                <div class="d-flex justify-content-start mb-4">
                    <div class="img_cont_msg">
                        <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1594610178/account_default1.jpg" alt="no image profil"
                            class="rounded-circle user_img_msg">
                    </div>
                    <div class="msg_cotainer">
                        `+ data +`
                        <span class="msg_time">baru saja</span>
                    </div>
                </div>`;

                $('.msg_card_body').append(html);
            }).fail(function () {
                alert("error");
            });
        });
    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<!--Coded With Love By Mutiullah Samim-->
<p id="result"></p>
<body>
    <div class="container-fluid h-100">
        <div class="row justify-content-center h-100">
            <div class="col-md-4 col-xl-3 chat">
                <div class="card mb-sm-3 mb-md-0 contacts_card">
                    <div class="card-header">
                        <div class="input-group">
                            <input type="text" placeholder="Search..." name="" class="form-control search">
                            <div class="input-group-prepend">
                                <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>
                    {{-- from me --}}
                    <div class="card-body contacts_body">
                        <input type="hidden" id="from_user_chat" value="{{ Auth()->user()->id }}">
                        <input type="hidden" id="from_user_chat_name" value="{{ Auth()->user()->name }}">
                        <ui class="contacts">
                            @foreach ($user as $u)
                            @if (Auth()->user()->name != $u['name'])
                            <li class="active" style="cursor: pointer;">
                                <div class="d-flex bd-highlight">
                                    <div class="img_cont">
                                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg"
                                            class="rounded-circle user_img">
                                            {{-- {{ dd(str_replace(' ', '',$u['name'])) }} --}}
                                        @if (in_array(str_replace(' ', '',$u['name']), $userOnline))
                                            <span class="online_icon"></span>
                                            <input type="hidden" id="status_{{ str_replace(' ', '',$u['name']) }}" value="online">
                                            @else
                                            <span class="offline_icon"></span>
                                            <input type="hidden" id="status_{{ str_replace(' ', '',$u['name']) }}" value="offline">
                                        @endif
                                    </div>
                                    <div class="user_info">
                                        <script>
                                            
                                        </script>
                                        <span>{{ $u['name'] }}</span>
                                        <input type="hidden" id="chat_{{ str_replace(' ', '',$u['name']) }}" value="{{ $u['id'] }}">
                                        {{-- <p>Kalid is online</p> --}}
                                    </div>
                                </div>
                            </li>
                            @endif
                            @endforeach
                            {{-- <li>
                                <div class="d-flex bd-highlight">
                                    <div class="img_cont">
                                        <img src="https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg"
                                            class="rounded-circle user_img">
                                        <span class="online_icon offline"></span>
                                    </div>
                                    <div class="user_info">
                                        <span>Taherah Big</span>
                                        <p>Taherah left 7 mins ago</p>
                                    </div>
                                </div>
                            </li> --}}
                        </ui>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            <div class="col-md-8 col-xl-6 chat">
                    <input type="hidden" class="user" value="{{ Auth()->user()->name }}">
                    <div class="card room-chat">
                    </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="user_online" style="display: none;" value="">


    <script>
        
    </script>
</body>

</html>
