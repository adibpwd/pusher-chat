<!DOCTYPE html>
<html>
<head>
  <title>Pusher Test</title>
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('9fc9cf0e1a216c7f7b7c', {
      cluster: 'ap1'
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function(data) {
      alert(JSON.stringify(data));
    });
  </script>
</head>
<body>
  <input type="text" id="event" placeholder="event">
  <input type="text" id="channel" placeholder="channel">
  <input type="submit" id="submit" value="submit">
  <script>
      $( () => {
        $('#submit').click( () => {
            let event = $('#event').val();
            let channel = $('#channel').val();

            channel = pusher.subscribe(channel);

            channel.bind(event, function(data) {
                alert(JSON.stringify(data));
            });
        });
      });
  </script>
</body>
</html>