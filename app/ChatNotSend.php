<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatNotSend extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
