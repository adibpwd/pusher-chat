<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\SendChat\SendChat;

class SendChatProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SendChat::class, function($app) {
            return new SendChat();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
